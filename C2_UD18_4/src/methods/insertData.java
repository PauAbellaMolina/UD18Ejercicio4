package methods;

import java.sql.SQLException;
import java.sql.Statement;

import dto.DatabaseDto;

public class insertData {

	public static void insertData(DatabaseDto nombre, String nombreTabla, String valorTabla) {
		
		try {
			
			String Querydb = "USE " + nombre.getNombre() + ";";
			Statement stdb = openConnection.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + nombreTabla + valorTabla;
			Statement st = openConnection.conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
			
		}
		
	}
	
}
