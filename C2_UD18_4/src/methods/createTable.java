package methods;

import java.sql.SQLException;
import java.sql.Statement;

import dto.*;

public class createTable {

	public static void createTable(DatabaseDto nombre, TableDto nombreTabla, String nombreValor) {
		
		try {
			
			String Querydb = "USE " + nombre.getNombre() + ";";
			Statement stdb = openConnection.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = nombreValor;
			Statement st = openConnection.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada con exito!");
			
		} catch (SQLException ex) {
			
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla");
			
		}
		
	}
	
}
