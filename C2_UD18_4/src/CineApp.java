import java.sql.Connection;
import methods.*;
import dto.*;

public class CineApp {
	
	public static Connection conexion;

	public static void main(String[] args) {
		
		openConnection.openConnection();
		
		DatabaseDto newDatabase = new DatabaseDto("Cine");
		createDB.createDB(newDatabase);
		
		TableDto newTable1 = new TableDto(newDatabase, "Peliculas");
		String newValue1 = "CREATE TABLE `Peliculas` (`Codigo` int NOT NULL, `Nombre` varchar(255) NOT NULL, `CalificacionEdad` varchar(255) DEFAULT NULL, PRIMARY KEY (`Codigo`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		createTable.createTable(newDatabase, newTable1, newValue1);
		insertData.insertData(newDatabase, "Peliculas", "(Codigo, Nombre, CalificacionEdad) VALUE(1, 'Citizen Kane', 10);");
		insertData.insertData(newDatabase, "Peliculas", "(Codigo, Nombre, CalificacionEdad) VALUE(2, 'Singing in the Rain', 5);");
		insertData.insertData(newDatabase, "Peliculas", "(Codigo, Nombre, CalificacionEdad) VALUE(3, 'The Wizard of Oz', 5);");
		insertData.insertData(newDatabase, "Peliculas", "(Codigo, Nombre, CalificacionEdad) VALUE(4, 'The Quiet Man', 13);");
		insertData.insertData(newDatabase, "Peliculas", "(Codigo, Nombre, CalificacionEdad) VALUE(5, 'North by Northwest', 13);");
		
		TableDto newTable2 = new TableDto(newDatabase, "Salas");
		String newValue2 = "CREATE TABLE `Salas` (`Codigo` int NOT NULL, `Nombre` varchar(255) NOT NULL, `Pelicula` int DEFAULT NULL, PRIMARY KEY (`Codigo`), KEY `Pelicula` (`Pelicula`), CONSTRAINT `Salas_ibfk_1` FOREIGN KEY (`Pelicula`) REFERENCES `Peliculas` (`Codigo`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		createTable.createTable(newDatabase, newTable2, newValue2);
		insertData.insertData(newDatabase, "Salas", "(Codigo, Nombre, Pelicula) VALUE(1, 'Odeon', 5);");
		insertData.insertData(newDatabase, "Salas", "(Codigo, Nombre, Pelicula) VALUE(2, 'Imperial', 1);");
		insertData.insertData(newDatabase, "Salas", "(Codigo, Nombre, Pelicula) VALUE(3, 'Majestic', 2);");
		insertData.insertData(newDatabase, "Salas", "(Codigo, Nombre, Pelicula) VALUE(4, 'Royale', 4);");
		insertData.insertData(newDatabase, "Salas", "(Codigo, Nombre, Pelicula) VALUE(5, 'Paraiso', 3);");
		
		closeConnection.closeConnection();

	}

}
